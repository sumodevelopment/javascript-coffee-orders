import { Sidebar } from "./js/sidebar.js";
import { Menu } from "./js/menu.js";
import { MOCK_DRINKS } from "./mocks/drinks.mocks.js";

class App {
    sidebar;
    menu;

    constructor() {
        this.sidebar = new Sidebar('sidebar-items', this.handleSidebarItemClick.bind(this))
        this.menu = new Menu('menu')
        this.menu.setMenuItems(MOCK_DRINKS.coffee, 'Coffee')
    }

    handleSidebarItemClick(menuId) {
        this.menu.setMenuItems(MOCK_DRINKS[menuId], menuId)
    }


    render() {
        this.sidebar.render()
    }
}

const app = new App();
app.render()
