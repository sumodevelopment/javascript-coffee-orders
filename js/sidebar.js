export class Sidebar {

    items = [
        {
            id: 1,
            name: 'Coffee',
            menuId: 'coffee'
        },
        {
            id: 2,
            name: 'Tea',
            menuId: 'tea'
        },
        {
            id: 3,
            name: 'Juice',
            menuId: 'juice'
        }
    ]

    constructor(element = 'sidebar-items', onItemClick) {
        this.elSidebar = document.getElementById(element)

        this.elSidebarItems = [];

        this.elSidebar.addEventListener('click',  event => {
            this.handleMenuItemsClick(event, onItemClick)
        })
    }

    handleMenuItemsClick(event, onItemClick) {
        const { target } = event
        const { menuId } = target.dataset
        const validMenuId = this.items.find(i => i.menuId === menuId)
        if (!validMenuId) {
            return
        }
        this.elSidebarItems.forEach(item => item.className = 'sidebar-items__item px py');
        target.className += ' active'
        onItemClick(menuId)
    }

    render() {
        for (const item of this.items) {
            const elItem = document.createElement('li')
            elItem.className = 'sidebar-items__item px py'
            elItem.id = item.id
            elItem.dataset.menuId = item.menuId
            elItem.innerText = item.name
            this.elSidebarItems.push(elItem)
            this.elSidebar.appendChild(elItem)
        }

        this.elSidebarItems[0].className += ' active';
    }
}
