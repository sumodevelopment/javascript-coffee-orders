export class Menu {

    menuItems = []
    menuTitle = ''

    constructor(element = 'menu-items') {
        this.elMenu = document.getElementById(element)
    }

    setMenuItems(items, title) {
        this.menuItems = items
        this.menuTitle = title
        this.render()
    }

    render() {
        // Clear current menu items.
        while (this.elMenu.hasChildNodes()) {
            this.elMenu.removeChild(this.elMenu.firstChild)
        }

        // Title
        const elMenuTitle = document.createElement('h2')
        elMenuTitle.className = 'menu-title'
        elMenuTitle.innerText = this.menuTitle
        this.elMenu.appendChild(elMenuTitle)


        // Items
        const elMenuItems = document.createElement('ul')
        elMenuItems.id = 'menu-items'
        elMenuItems.className = 'grid menu-grid'

        for (const item of this.menuItems) {
            const elItem = document.createElement('article')
            elItem.className = 'menu-items__item'

            const elItemImage = new Image()
            elItemImage.src = item.image
            elItemImage.className = 'menu-items__item-image'

            const elItemName = document.createElement('h4')
            elItemImage.innerText = item.name

            const elItemDescription = document.createElement('p')
            elItemDescription.innerText = item.description

            const elItemPrice = document.createElement('span')
            elItemPrice.innerText = item.basePrice

            const elItemQty = document.createElement('span')
            elItemQty.innerText = 1

            const elItemActions = document.createElement('footer')

            const elItemAdd = document.createElement('button')
            elItemAdd.className = 'btn btn-primary'
            elItemAdd.innerText = '+'

            const elItemRemove = document.createElement('button')
            elItemRemove.className = 'btn btn-danger'
            elItemRemove.innerText = '-'

            elItemActions.appendChild(elItemRemove)
            elItemActions.appendChild(elItemQty)
            elItemActions.appendChild(elItemAdd)

            elItem.appendChild(elItemImage)
            elItem.appendChild(elItemName)
            elItem.appendChild(elItemDescription)
            elItem.appendChild(elItemPrice)

            elItem.appendChild(elItemActions)

            elMenuItems.appendChild(elItem)
        }

        this.elMenu.appendChild(elMenuItems)
    }
}
