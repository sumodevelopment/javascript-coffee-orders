export const MOCK_DRINKS = {
    coffee: [
        {
            id: 1,
            name: 'Americano',
            image: 'img/coffee/1.jpg',
            description: 'A strong coffee made with espresso and hot water.',
            basePrice: 28,
        },
        {
            id: 2,
            name: 'Cappuccino',
            image: 'img/coffee/2.jpg',
            description: 'Classic coffee with lots of frothed milk and a shot of espresso.',
            basePrice: 32,
        },
        {
            id: 3,
            name: 'Latte',
            image: 'img/coffee/3.jpg',
            description: 'Milky coffee with a shot of espresso.',
            basePrice: 38,
        },
        {
            id: 4,
            name: 'Decaf Coffee',
            image: 'img/coffee/4.jpg',
            description: 'It has the taste but not the kick.',
            basePrice: 30
        }
    ],
    tea: [
        {
            id: 1,
            name: 'Ceylon',
            image: 'img/tea/1.jpg',
            description: 'Traditional english tea',
            basePrice: 22,
        },
        {
            id: 2,
            name: 'Earl Grey',
            image: 'img/tea/2.jpg',
            description: 'A black tea with bergamot',
            basePrice: 28,
        },
        {
            id: 3,
            name: 'Mint and Rose',
            image: 'img/tea/3.jpg',
            description: 'Soft minty tea, great for relaxation',
            basePrice: 32,
        },
        {
            id: 4,
            name: 'Chai Latte',
            image: 'img/tea/4.jpg',
            description: 'Spicy tea with milk.',
            basePrice: 38
        }
    ],
    juice: [
        {
            id: 1,
            name: 'Pink Drink',
            image: 'img/juice/1.jpg',
            description: 'Strawberry with Passion Fruit',
            basePrice: 34
        },
        {
            id: 2,
            name: 'Purple Mango',
            image: 'img/juice/2.jpg',
            description: 'Tropical Mango juice with hints of Dragonfruit',
            basePrice: 36
        },
        {
            id: 3,
            name: 'Fresh Kiwi',
            image: 'img/juice/3.jpg',
            description: 'Starfruit- and kiwi-flavored juice with lemonade.',
            basePrice: 38
        },
        {
            id: 4,
            name: 'Smooth Pineapple',
            image: 'img/juice/4.jpg',
            description: 'Smoothie style Pineapple juice.',
            basePrice: 38
        },
    ]
}
